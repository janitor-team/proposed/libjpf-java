Source: libjpf-java
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: tony mancill <tmancill@debian.org>
Section: java
Priority: optional
Build-Depends: debhelper-compat (= 13),
               ant,
               javahelper,
               maven-repo-helper
Build-Depends-Indep: default-jdk,
                     libcommons-logging-java,
                     libjxp-java,
                     libonemind-commons-java-java,
                     libonemind-commons-invoke-java
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/java-team/libjpf-java
Vcs-Git: https://salsa.debian.org/java-team/libjpf-java.git
Homepage: http://jpf.sourceforge.net/

Package: libjpf-java
Architecture: all
Depends: ${misc:Depends}
Description: Java Plugin Framework: plug-in infrastructure library for Java projects
 JPF provides a runtime engine that dynamically discovers and loads
 "plug-ins". A plug-in is a structured component that describes itself to JPF
 using a "manifest". JPF maintains a registry of available plug-ins and the
 functions they provide (via extension points and extensions).
 .
 One major goal of JPF is that the application (and its end-user) should not
 pay any memory or performance penalty for plug-ins that are installed, but
 not used. Plug-ins are added to the registry at application start-up or
 while the application is running but they are not loaded until they are
 called.
